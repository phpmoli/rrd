#!/bin/bash

#rrd_debug=true


if ! find . -name rrd.sh >/dev/null 2>/dev/null
then
  echo 'ERROR cannot find self, pwd must be scripts directory' 1>&2
  exit 1
fi

rrd_sharedir="/dev/shm"
rrd_hostname="$HOSTNAME"
rrd_httpd_port=8080
rrd_httpd_auth=''
rrd_idokep_users=( 'kosztam' )
rrd_data_server='http://192.168.0.3:8080/download?auth=secret'
rrd_ohm_server='http://192.168.0.4:8085'
rrd_tplink_address='192.168.0.1'
rrd_tplink_auth='routerAdminUserName:routerAdminPassword'

file=.rrd-config.sh
if test -f "$file"
then source "$file"
fi


rrd_logfile="rrd-$1.log"

mkdir "$rrd_sharedir/rrd" 2>/dev/null
chmod g+w "$rrd_sharedir/rrd" 2>/dev/null
touch "$rrd_sharedir/rrd/.placeholder" 2>/dev/null

function rrd_checkcmd {
  if ! which "$1" >/dev/null
  then
    echo "ERROR missing command: $1" 1>&2
    exit 1
  fi
}

function rrd_update {
  date='N'
  if [ -n "$1" ]
  then date="$1"
  fi
  if [ "$rrd_once" = 'true' ]
  then echo -e "$rrd_hostname-$rrd_probe\\t$date\\t$data"
  fi
  rrdtool update "$rrd_hostname-$rrd_probe.rrd" $date:$data 2>/dev/null
}

function rrd_error {
  echo -e "$(date '+%Y-%m-%d %H:%M:%S')\tERROR\t$rrd_probe\t\"$input\"" | tee -a "$rrd_logfile" 1>&2
}

case "$1" in



  'graph_graph')
    if [ -n "$2" ]
    then rrd_hostname="$2"
    fi

    $0 graph > "$rrd_sharedir/rrd-graph-$rrd_hostname.png"
    xdg-open "$rrd_sharedir/rrd-graph-$rrd_hostname.png"
  ;;



  'graph')
    if [ -n "$2" ]
    then rrd_hostname="$2"
    fi


    case "$rrd_hostname" in
      'moli-beebox')
        rrd_graph_starts=('-90min' '-12h' '-4d' '-32d' '-1y')
        rrd_graphs=('ram' 'load' "temp-$rrd_hostname" 'humid' 'torrent')
        rrd_graph_legend_width=12
        rrd_graph_ram_height=60
        rrd_graph_ram_total=6
        rrd_graph_load_height=90
        rrd_graph_temp_height=200
        rrd_graph_torrent_height=150
      ;;
      'moli-desktop')
        rrd_graph_starts=('-2h' '-1d' '-1w')
        rrd_graphs=('ram' 'load' 'windows' "temp-$rrd_hostname" 'humid' "fan-$rrd_hostname")
        rrd_graph_legend_width=12
        rrd_graph_ram_height=80
        rrd_graph_ram_total=32
        rrd_graph_load_height=90
        rrd_graph_windows_height=90
        rrd_graph_temp_height=180
        rrd_graph_fan_height=120
      ;;
      'net')
        rrd_graph_starts=('-12h' '-4d' '-32d')
        rrd_graphs=('traffic')
        rrd_graph_legend_width=4
        rrd_graph_traffic_height=150
      ;;
    esac

    rrd_graph_humid_height=110
    rrd_graph_width=1280
    rrd_graph_divider_height=30
    rrd_graph_opts='--full-size-mode --border 0 --legend-position=west --font WATERMARK:4 --units-length 1 --imgformat PNG --interlaced --legend-direction=bottomup --dynamic-labels --right-axis 1:0 --slope-mode'

    rrd_sharedir="$rrd_sharedir/rrd-graph-cache-$(dd if=/dev/urandom bs=1 count=128 2>/dev/null | sha512sum --binary | cut -b -128)"
    mkdir "$rrd_sharedir"

    for i in $(seq $rrd_graph_legend_width 2>/dev/null)
    do rrd_graph_legend_spacer="$rrd_graph_legend_spacer "
    done

    rrd_checkcmd convert

    for graph in "${rrd_graphs[@]}"
    do
      case "$graph" in
        'temp-moli-beebox')
          rrd_graph_axis_lower=-15
          rrd_graph_axis_upper=80

          i=0
          for start in "${rrd_graph_starts[@]}"
          do
            if [ -n "$start" ]
            then
              i=$((i+1))
              rrdtool $1 "$rrd_sharedir/${graph}_$i.png" --start "$start" $rrd_graph_opts --width "$rrd_graph_width" --height "$rrd_graph_temp_height" --rigid --lower-limit "$rrd_graph_axis_lower" --upper-limit "$rrd_graph_axis_upper" --base 1000 --units-exponent 0 --right-axis-format '%2.0lf' \
                DEF:hdd-temp="$rrd_hostname-hdd-temp.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:hdd-vid-temp="$rrd_hostname-hdd-vid-temp.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:sys-temp1="$rrd_hostname-sys-temp1.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:sys-temp2="$rrd_hostname-sys-temp2.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:sys-temp3="$rrd_hostname-sys-temp3.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:cpu-temp="$rrd_hostname-cpu-temp.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:cpu1-temp="$rrd_hostname-cpu1-temp.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:cpu2-temp="$rrd_hostname-cpu2-temp.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:cpu3-temp="$rrd_hostname-cpu3-temp.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:cpu4-temp="$rrd_hostname-cpu4-temp.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:indoor-temp="$rrd_hostname-indoor-temp.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:indoor-temp2="$rrd_hostname-indoor-temp2.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:outdoor-temp="$rrd_hostname-outdoor-temp.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:outdoor-temp2="$rrd_hostname-outdoor-temp2.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:outdoor-temp3="$rrd_hostname-outdoor-temp3.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:outdoor-temp4="$rrd_hostname-outdoor-temp4.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:outdoor-temp5="$rrd_hostname-outdoor-temp5.rrd":data:AVERAGE:reduce=AVERAGE \
                CDEF:indoor-temp-calib=indoor-temp,4,- \
                COMMENT:"$rrd_graph_legend_spacer\s" \
                HRULE:0#228B22::dashes \
                HRULE:40#8B0000::dashes \
                LINE1:outdoor-temp5#228B22 \
                LINE1:outdoor-temp4#228B22 \
                LINE1:outdoor-temp3#228B22 \
                LINE1:outdoor-temp2#228B22 \
                LINE1:outdoor-temp#228B22:"Outdoor\l" \
                LINE2:indoor-temp2#4169E1 \
                LINE2:indoor-temp-calib#4169E1:"Indoor\l" \
                LINE1:cpu4-temp#B8860B \
                LINE1:cpu3-temp#B8860B \
                LINE1:cpu2-temp#B8860B \
                LINE1:cpu1-temp#B8860B \
                LINE1:cpu-temp#B8860B:"CPU\l" \
                LINE1:sys-temp3#808000 \
                LINE1:sys-temp2#808000 \
                LINE1:sys-temp1#808000:"System\l" \
                LINE2:hdd-vid-temp#FF1493:"HDD vid\l" \
                LINE2:hdd-temp#FF0000:"HDD sys\l" \
                COMMENT:"Temperatures\l" \
                >/dev/null
            fi
          done
        ;;
        'temp-moli-desktop')
          rrd_graph_axis_lower=-15
          rrd_graph_axis_upper=85

          i=0
          for start in "${rrd_graph_starts[@]}"
          do
            if [ -n "$start" ]
            then
              i=$((i+1))
              rrdtool $1 "$rrd_sharedir/${graph}_$i.png" --start "$start" $rrd_graph_opts --width "$rrd_graph_width" --height "$rrd_graph_temp_height" --rigid --lower-limit "$rrd_graph_axis_lower" --upper-limit "$rrd_graph_axis_upper" --base 1000 --units-exponent 0 --right-axis-format '%2.0lf' \
                DEF:hdd-dat-temp="$rrd_hostname-hdd-dat-temp.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:ssd-temp="$rrd_hostname-ssd-temp.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:gpu-temp="$rrd_hostname-gpu-temp.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:cpu-temp="$rrd_hostname-cpu-temp.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:cpu1-temp="$rrd_hostname-cpu1-temp.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:cpu2-temp="$rrd_hostname-cpu2-temp.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:cpu3-temp="$rrd_hostname-cpu3-temp.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:cpu4-temp="$rrd_hostname-cpu4-temp.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:sys-temp1="$rrd_hostname-sys-temp1.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:sys-temp2="$rrd_hostname-sys-temp2.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:sys-temp3="$rrd_hostname-sys-temp3.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:indoor-temp="$rrd_hostname-indoor-temp.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:indoor-temp2="$rrd_hostname-indoor-temp2.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:outdoor-temp="$rrd_hostname-outdoor-temp.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:outdoor-temp2="$rrd_hostname-outdoor-temp2.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:outdoor-temp3="$rrd_hostname-outdoor-temp3.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:outdoor-temp4="$rrd_hostname-outdoor-temp4.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:outdoor-temp5="$rrd_hostname-outdoor-temp5.rrd":data:AVERAGE:reduce=AVERAGE \
                CDEF:cpu-temp-calib=cpu-temp,8,+ \
                CDEF:cpu1-temp-calib=cpu1-temp,12,+ \
                CDEF:cpu2-temp-calib=cpu2-temp,10,+ \
                CDEF:cpu3-temp-calib=cpu3-temp,12,+ \
                CDEF:cpu4-temp-calib=cpu4-temp,12,+ \
                CDEF:sys-temp1-calib=sys-temp1,4,+ \
                CDEF:sys-temp3-calib=sys-temp3,8,+ \
                CDEF:indoor-temp-calib=indoor-temp,4,- \
                COMMENT:"$rrd_graph_legend_spacer\s" \
                HRULE:0#228B22::dashes \
                HRULE:40#8B0000::dashes \
                LINE1:outdoor-temp5#228B22 \
                LINE1:outdoor-temp4#228B22 \
                LINE1:outdoor-temp3#228B22 \
                LINE1:outdoor-temp2#228B22 \
                LINE1:outdoor-temp#228B22:"Outdoor\l" \
                LINE2:indoor-temp2#4169E1 \
                LINE2:indoor-temp-calib#4169E1:"Indoor\l" \
                LINE1:cpu4-temp-calib#B8860B \
                LINE1:cpu3-temp-calib#B8860B \
                LINE1:cpu2-temp-calib#B8860B \
                LINE1:cpu1-temp-calib#B8860B \
                LINE1:cpu-temp-calib#B8860B:"CPU\l" \
                LINE1:sys-temp3-calib#808000 \
                LINE1:sys-temp2#808000 \
                LINE1:sys-temp1-calib#808000:"System\l" \
                LINE2:gpu-temp#363636:"GPU\l" \
                LINE2:hdd-dat-temp#FF1493:"HDD dat\l" \
                LINE2:ssd-temp#FF0000:"SSD\l" \
                COMMENT:"Temperatures\l" \
                >/dev/null
            fi
          done
        ;;
        'humid')
          rrd_graph_axis_lower=0
          rrd_graph_axis_upper=100

          i=0
          for start in "${rrd_graph_starts[@]}"
          do
            if [ -n "$start" ]
            then
              i=$((i+1))
              rrdtool $1 "$rrd_sharedir/${graph}_$i.png" --start "$start" $rrd_graph_opts --width "$rrd_graph_width" --height "$rrd_graph_humid_height" --rigid --lower-limit "$rrd_graph_axis_lower" --upper-limit "$rrd_graph_axis_upper" --base 1000 --units-exponent 0 --right-axis-format '%3.0lf' \
                DEF:outdoor-humid="$rrd_hostname-outdoor-humid.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:outdoor-humid2="$rrd_hostname-outdoor-humid2.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:outdoor-humid3="$rrd_hostname-outdoor-humid3.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:outdoor-humid4="$rrd_hostname-outdoor-humid4.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:outdoor-humid5="$rrd_hostname-outdoor-humid5.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:indoor-humid="$rrd_hostname-indoor-humid.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:indoor-humid-min="$rrd_hostname-indoor-humid.rrd":data:MIN:reduce=MIN \
                DEF:indoor-humid-max="$rrd_hostname-indoor-humid.rrd":data:MAX:reduce=MAX \
                COMMENT:"$rrd_graph_legend_spacer\s" \
                LINE1:outdoor-humid5#008080 \
                LINE1:outdoor-humid4#008080 \
                LINE1:outdoor-humid3#008080 \
                LINE1:outdoor-humid2#008080 \
                LINE1:outdoor-humid#008080:"Outdoor\l" \
                AREA:indoor-humid#5F9EA0C0:"Indoor\l" \
                LINE1:indoor-humid-min#5F9EA0 \
                LINE1:indoor-humid-max#5F9EA0 \
                COMMENT:"Humidity\l" \
                >/dev/null
            fi
          done
        ;;
        'fan-moli-desktop')
          rrd_graph_axis_lower=0
          rrd_graph_axis_upper=5600

          i=0
          for start in "${rrd_graph_starts[@]}"
          do
            if [ -n "$start" ]
            then
              i=$((i+1))
              rrdtool $1 "$rrd_sharedir/${graph}_$i.png" --start "$start" $rrd_graph_opts --width "$rrd_graph_width" --height "$rrd_graph_fan_height" --rigid --lower-limit "$rrd_graph_axis_lower" --upper-limit "$rrd_graph_axis_upper" --base 1000 --units-exponent 0 --right-axis-format '%4.0lf' \
                DEF:gpu-rpm="$rrd_hostname-gpu-rpm.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:sys-rpm1="$rrd_hostname-sys-rpm1.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:sys-rpm2="$rrd_hostname-sys-rpm2.rrd":data:AVERAGE:reduce=AVERAGE \
                COMMENT:"$rrd_graph_legend_spacer\s" \
                LINE2:sys-rpm2#800080 \
                LINE2:sys-rpm1#800080:"Sys RPM\l" \
                LINE2:gpu-rpm#000000:"GPU RPM\l" \
                COMMENT:"Cooling\l" \
                >/dev/null
            fi
          done
        ;;
        'ram')
          rrd_graph_axis_lower=0
          rrd_graph_axis_upper="$rrd_graph_ram_total"

          i=0
          for start in "${rrd_graph_starts[@]}"
          do
            if [ -n "$start" ]
            then
              i=$((i+1))
              rrdtool $1 "$rrd_sharedir/${graph}_$i.png" --start "$start" $rrd_graph_opts --width "$rrd_graph_width" --height "$rrd_graph_ram_height" --rigid --lower-limit "$rrd_graph_axis_lower" --upper-limit "$rrd_graph_axis_upper" --base 1000 --units-exponent 0 --right-axis-format '%2.0lf' \
                DEF:ram-used="$rrd_hostname-ram-used.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:ram-used-max="$rrd_hostname-ram-used.rrd":data:MAX:reduce=MAX \
                DEF:swap-used="$rrd_hostname-swap-used.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:swap-used-max="$rrd_hostname-swap-used.rrd":data:MAX:reduce=MAX \
                CDEF:ram-used-gb=ram-used,1024,/,1024,/,1024,/ \
                CDEF:ram-used-max-gb=ram-used-max,1024,/,1024,/,1024,/ \
                CDEF:swap-used-gb=swap-used,1024,/,1024,/,1024,/ \
                CDEF:swap-used-max-gb=swap-used-max,1024,/,1024,/,1024,/ \
                COMMENT:"$rrd_graph_legend_spacer\s" \
                LINE1:ram-used-max-gb#6B8E23 \
                AREA:ram-used-gb#6B8E23:"RAM used\l" \
                LINE1:swap-used-max-gb#32CD32 \
                AREA:swap-used-gb#32CD3280:"Swap used\l" \
                >/dev/null
            fi
          done
        ;;
        'load')
          rrd_graph_axis_lower=0
          rrd_graph_axis_upper=4

          i=0
          for start in "${rrd_graph_starts[@]}"
          do
            if [ -n "$start" ]
            then
              i=$((i+1))
              rrdtool $1 "$rrd_sharedir/${graph}_$i.png" --start "$start" $rrd_graph_opts --width "$rrd_graph_width" --height "$rrd_graph_load_height" --lower-limit "$rrd_graph_axis_lower" --upper-limit "$rrd_graph_axis_upper" --base 1000 --units-exponent 0 --right-axis-format '%2.0lf' \
                DEF:cpu-load1="$rrd_hostname-cpu-load1.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:cpu-load1-max="$rrd_hostname-cpu-load1.rrd":data:MAX:reduce=MAX \
                COMMENT:"$rrd_graph_legend_spacer\s" \
                HRULE:"$rrd_graph_axis_upper"#CD5C5C::dashes \
                LINE1:cpu-load1-max#CD5C5C \
                AREA:cpu-load1#CD5C5CC0:"1 minute\l" \
                COMMENT:"Load average\l" \
                >/dev/null
            fi
          done
        ;;
        'windows')
          rrd_graph_axis_lower=0
          rrd_graph_axis_upper=100

          i=0
          for start in "${rrd_graph_starts[@]}"
          do
            if [ -n "$start" ]
            then
              i=$((i+1))
              rrdtool $1 "$rrd_sharedir/${graph}_$i.png" --start "$start" $rrd_graph_opts --width "$rrd_graph_width" --height "$rrd_graph_windows_height" --lower-limit "$rrd_graph_axis_lower" --upper-limit "$rrd_graph_axis_upper" --base 1000 --units-exponent 0 --right-axis-format '%2.0lf' \
                DEF:cpu-used="$rrd_hostname-cpu-used.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:cpu-used-max="$rrd_hostname-cpu-used.rrd":data:MAX:reduce=MAX \
                DEF:gpu-used="$rrd_hostname-gpu-used.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:gpu-used-max="$rrd_hostname-gpu-used.rrd":data:MAX:reduce=MAX \
                COMMENT:"$rrd_graph_legend_spacer\s" \
                LINE1:cpu-used-max#008080 \
                AREA:cpu-used#00808080:"CPU used\l" \
                LINE1:gpu-used-max#808080 \
                AREA:gpu-used#80808080:"GPU used\l" \
                COMMENT:"Windows\l" \
                >/dev/null
            fi
          done
        ;;
        'torrent')
          rrd_graph_axis_upper=100

          i=0
          for start in "${rrd_graph_starts[@]}"
          do
            if [ -n "$start" ]
            then
              i=$((i+1))
              rrdtool $1 "$rrd_sharedir/${graph}_$i.png" --start "$start" $rrd_graph_opts --width "$rrd_graph_width" --height "$rrd_graph_torrent_height" --rigid --lower-limit 0 --upper-limit "$rrd_graph_axis_upper" --base 1000 --units-exponent 0 --right-axis-format '%3.0lf' \
                DEF:torrent-up="$rrd_hostname-torrent-up.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:torrent-up-max="$rrd_hostname-torrent-up.rrd":data:MAX:reduce=MAX \
                DEF:torrent-down="$rrd_hostname-torrent-down.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:torrent-down-max="$rrd_hostname-torrent-down.rrd":data:MAX:reduce=MAX \
                DEF:net-google="$rrd_hostname-net-google.rrd":data:AVERAGE:reduce=AVERAGE \
                CDEF:torrent-up-mbit=torrent-up,8,*,1024,/,1024,/ \
                CDEF:torrent-up-max-mbit=torrent-up-max,8,*,1024,/,1024,/ \
                CDEF:torrent-down-mbit=torrent-down,8,*,1024,/,1024,/ \
                CDEF:torrent-down-max-mbit=torrent-down-max,8,*,1024,/,1024,/ \
                CDEF:net-google-prop=net-google,"$rrd_graph_axis_upper",0,-,*,100,/,0,+ \
                COMMENT:"$rrd_graph_legend_spacer\s" \
                AREA:net-google-prop#FFD70040:"google\l" \
                LINE1:torrent-up-max-mbit#778899 \
                AREA:torrent-up-mbit#77889980:"up\l" \
                LINE1:torrent-down-max-mbit#2F4F4F \
                AREA:torrent-down-mbit#2F4F4F80:"down\l" \
                >/dev/null
            fi
          done
        ;;
        'traffic')
          rrd_graph_axis_upper=100

          i=0
          for start in "${rrd_graph_starts[@]}"
          do
            if [ -n "$start" ]
            then
              i=$((i+1))
              rrdtool $1 "$rrd_sharedir/${graph}_$i.png" --start "$start" $rrd_graph_opts --width "$rrd_graph_width" --height "$rrd_graph_traffic_height" --rigid --lower-limit 0 --upper-limit "$rrd_graph_axis_upper" --base 1000 --units-exponent 0 --right-axis-format '%3.0lf' \
                DEF:net-down="$rrd_hostname-down.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:net-down-max="$rrd_hostname-down.rrd":data:MAX:reduce=MAX \
                DEF:net-up="$rrd_hostname-up.rrd":data:AVERAGE:reduce=AVERAGE \
                DEF:net-up-max="$rrd_hostname-up.rrd":data:MAX:reduce=MAX \
                DEF:net-digi="$rrd_hostname-digi.rrd":data:AVERAGE:reduce=AVERAGE \
                CDEF:net-down-mbit=net-down,8,*,1024,/,1024,/ \
                CDEF:net-down-max-mbit=net-down-max,8,*,1024,/,1024,/ \
                CDEF:net-up-mbit=net-up,8,*,1024,/,1024,/ \
                CDEF:net-up-max-mbit=net-up-max,8,*,1024,/,1024,/ \
                CDEF:net-digi-prop=net-digi,"$rrd_graph_axis_upper",0,-,*,100,/,0,+ \
                COMMENT:"$rrd_graph_legend_spacer\s" \
                AREA:net-digi-prop#FFD70040:"digi\l" \
                LINE1:net-up-max-mbit#778899 \
                AREA:net-up-mbit#77889980:"up\l" \
                LINE1:net-down-max-mbit#2F4F4F \
                AREA:net-down-mbit#2F4F4F80:"down\l" \
                >/dev/null
            fi
          done
        ;;
      esac
    done

    if [ ${#rrd_graph_starts[@]} -gt 0 -a ${#rrd_graphs[@]} -gt 0 ]
    then
      convert -size ${rrd_graph_width}x$rrd_graph_divider_height xc:transparent "$rrd_sharedir/-.png"
      rrd_graph_cmd_init='convert -append'
      rrd_graph_cmd2="$rrd_graph_cmd_init"
      i=0
      for start in "${rrd_graph_starts[@]}"
      do
        if [ -n "$start" ]
        then
          rrd_graph_cmd1="$rrd_graph_cmd_init"
          i=$((i+1))
          for graph in "${rrd_graphs[@]}"
          do
            if [ -n "$graph" ]
            then rrd_graph_cmd1="$rrd_graph_cmd1 $rrd_sharedir/${graph}_$i.png"
            fi
          done
          if [ $i -lt ${#rrd_graph_starts[@]} ]
          then rrd_graph_cmd1="$rrd_graph_cmd1 $rrd_sharedir/-.png"
          fi
          file="$rrd_sharedir/$i.png"
          rrd_graph_cmd1="$rrd_graph_cmd1 $file"
          $rrd_graph_cmd1
          rrd_graph_cmd2="$rrd_graph_cmd2 $file"
        fi
      done
      rrd_graph_cmd2="$rrd_graph_cmd2 -"
      $rrd_graph_cmd2
    fi

    rm --recursive "$rrd_sharedir"
  ;;



  'create')
    case "$rrd_hostname" in
      'moli-beebox')
        rrd_gauges_avg=( 'moli-beebox-indoor-temp' 'moli-beebox-indoor-temp2' \
                         'moli-beebox-outdoor-temp' 'moli-beebox-outdoor-temp2' 'moli-beebox-outdoor-temp3' 'moli-beebox-outdoor-temp4' 'moli-beebox-outdoor-temp5' \
                         'moli-beebox-outdoor-humid' 'moli-beebox-outdoor-humid2' 'moli-beebox-outdoor-humid3' 'moli-beebox-outdoor-humid4' 'moli-beebox-outdoor-humid5' \
                         'moli-beebox-net-google' \
                         'moli-desktop-indoor-temp' 'moli-desktop-indoor-temp2' \
                         'moli-desktop-outdoor-temp' 'moli-desktop-outdoor-temp2' 'moli-desktop-outdoor-temp3' 'moli-desktop-outdoor-temp4' 'moli-desktop-outdoor-temp5' \
                         'moli-desktop-outdoor-humid' 'moli-desktop-outdoor-humid2' 'moli-desktop-outdoor-humid3' 'moli-desktop-outdoor-humid4' 'moli-desktop-outdoor-humid5' \
                         'net-digi' \
                         'moli-beebox-torrent-ratio' )
        rrd_gauges_avgmax=( 'moli-beebox-hdd-temp' 'moli-beebox-hdd-vid-temp' \
                            'moli-beebox-cpu-temp' 'moli-beebox-cpu1-temp' 'moli-beebox-cpu2-temp' 'moli-beebox-cpu3-temp' 'moli-beebox-cpu4-temp' \
                            'moli-beebox-sys-temp1' 'moli-beebox-sys-temp2' 'moli-beebox-sys-temp3' \
                            'moli-beebox-ram-used' 'moli-beebox-swap-used' 'moli-beebox-cpu-load1' \
                            'moli-desktop-hdd-dat-temp' 'moli-desktop-ssd-temp' \
                            'moli-desktop-gpu-temp' \
                            'moli-desktop-cpu-temp' 'moli-desktop-cpu1-temp' 'moli-desktop-cpu2-temp' 'moli-desktop-cpu3-temp' 'moli-desktop-cpu4-temp' \
                            'moli-desktop-sys-temp1' 'moli-desktop-sys-temp2' 'moli-desktop-sys-temp3' \
                            'moli-desktop-gpu-rpm' 'moli-desktop-sys-rpm1' 'moli-desktop-sys-rpm2' \
                            'moli-desktop-ram-used' 'moli-desktop-swap-used' 'moli-desktop-cpu-load1' 'moli-desktop-cpu-used' 'moli-desktop-gpu-used' )
        rrd_gauges_avgminmax=( 'moli-beebox-indoor-humid' 'moli-desktop-indoor-humid' )
        rrd_derives_avgmax=( 'moli-beebox-torrent-down' 'moli-beebox-torrent-up' )
        rrd_derives_avgmax0=( 'net-down' 'net-up' )
      ;;
    esac


    for cf in 'AVERAGE' 'MIN' 'MAX'
    do
      varname=rrd_def$cf
      printf -v $varname " \
        RRA:$cf:0.999:1:4000 \
        RRA:$cf:0.999:5:4000 \
        RRA:$cf:0.999:15:4000 \
        RRA:$cf:0.999:60:4000 \
        RRA:$cf:0.999:300:4000 \
        RRA:$cf:0.999:900:4000 \
        RRA:$cf:0.999:3600:4000 \
        RRA:$cf:0.999:10800:4000 \
        RRA:$cf:0.999:86400:4000"
    done

    for gauge in "${rrd_gauges_avg[@]}"
    do
      if [ -n "$gauge" -a ! -f "$gauge.rrd" ]
      then rrdtool $1 "$gauge.rrd" --no-overwrite --step 1 DS:data:GAUGE:1000:U:U $rrd_defAVERAGE
      fi
    done
    for gauge in "${rrd_gauges_avgmax[@]}"
    do
      if [ -n "$gauge" -a ! -f "$gauge.rrd" ]
      then rrdtool $1 "$gauge.rrd" --no-overwrite --step 1 DS:data:GAUGE:1000:U:U $rrd_defAVERAGE $rrd_defMAX
      fi
    done
    for gauge in "${rrd_gauges_avgminmax[@]}"
    do
      if [ -n "$gauge" -a ! -f "$gauge.rrd" ]
      then rrdtool $1 "$gauge.rrd" --no-overwrite --step 1 DS:data:GAUGE:1000:U:U $rrd_defAVERAGE $rrd_defMIN $rrd_defMAX
      fi
    done
    for gauge in "${rrd_derives_avgmax[@]}"
    do
      if [ -n "$gauge" -a ! -f "$gauge.rrd" ]
      then rrdtool $1 "$gauge.rrd" --no-overwrite --step 1 DS:data:DERIVE:1000:U:U $rrd_defAVERAGE $rrd_defMAX
      fi
    done
    for gauge in "${rrd_derives_avgmax0[@]}"
    do
      if [ -n "$gauge" -a ! -f "$gauge.rrd" ]
      then rrdtool $1 "$gauge.rrd" --no-overwrite --step 1 DS:data:DERIVE:1000:0:U $rrd_defAVERAGE $rrd_defMAX
      fi
    done
  ;;



  'flushcached')
    if test -f /etc/default/rrdcached
    then source /etc/default/rrdcached
    fi

    if ! [ -n "$RRDCACHED_ADDRESS" ]
    then
      if [ -n "$SOCKFILE" ]
      then export RRDCACHED_ADDRESS="unix:$SOCKFILE"
      elif test -n "$NETWORK_OPTIONS" -a "$NETWORK_OPTIONS" = '-L'
      then export RRDCACHED_ADDRESS="localhost"
      fi
    fi

    find . -name "$rrd_hostname-*.rrd" 2>/dev/null | while read -r file
    do
      if [ -n "$RRDCACHED_ADDRESS" ]
      then rrdtool $1 "$file" 2>/dev/null
      fi
      if [ -n "$rrd_debug" ]
      then cp "$file" "$file~"
      fi
    done
  ;;



  'recover')
    if [ "$2" = 'sure' ]
    then
      find . -name "$rrd_hostname-*.rrd~" | while read -r file
      do
        file=$(basename "$file" '.rrd~')
        mv "$file.rrd~" "$file.rrd"
      done
    fi
  ;;



  'httpd')
    rrd_httpddir="/tmp/rrd"
    mkdir --parents "$rrd_httpddir" 2>/dev/null
    chmod 770 "$rrd_httpddir"
    rrd_httpdfile="$rrd_httpddir/rrd-$1.js"
    rrd_sharedir="$rrd_sharedir/rrd"

    rrd_checkcmd nodejs

    echo -n > "$rrd_httpdfile"
    if [ -n "$rrd_debug" ]
    then echo "debug = true;" > "$rrd_httpdfile"
    fi

    cat << HEREDOC >> "$rrd_httpdfile"
debug = (typeof debug !== "undefined" && debug ? true : false);
http_createserver = require("http").createServer;
crypto_randombytes = require("crypto").pseudoRandomBytes;
url_parse = require("url").parse;
child_process_exec = require("child_process").exec;
os_hostname = require("os").hostname;
fs_existssync = require("fs").existsSync;
fs_readfilesync = require("fs").readFileSync;
var server = http_createserver (
  function (IncomingMessage, ServerResponse) {
    if (IncomingMessage.url == "/favicon.ico") {
      ServerResponse.statusCode = 410;
      ServerResponse.end ();
    } else {
      var url = url_parse(IncomingMessage.url, true);
      var querystring = url.query;
      if ("$rrd_httpd_auth" && querystring.auth != "$rrd_httpd_auth" && IncomingMessage.connection.remoteAddress != "127.0.0.1") {
        ServerResponse.statusCode = 403;
        ServerResponse.end ();
        console.info (new Date().toISOString() + "\tnoauth: " + IncomingMessage.url + "\tremoteaddr: " + IncomingMessage.connection.remoteAddress);
      } else {
        switch (url.pathname) {
          case "/ping":
            ServerResponse.statusCode = 204;
            ServerResponse.end ();
            if (debug) console.info (new Date().toISOString() + "\tpong\tremoteaddr: " + IncomingMessage.connection.remoteAddress);
            break;
          case "/download":
            if (!querystring.file || !querystring.file.match (/^[0-9a-z_\-][0-9a-z_\.\-]*\.(tsv|rrd)$/i)) {
              ServerResponse.statusCode = 400;
              ServerResponse.end ();
              console.info (new Date().toISOString() + "\tdownload invalid: " + IncomingMessage.url + "\tremoteaddr: " + IncomingMessage.connection.remoteAddress);
            } else {
              if (! [ "$rrd_sharedir", "." ].some (
                function (element, index, array) {
                  if (element && fs_existssync (element + "/" + querystring.file)) {
                    path = element;
                    return true;
                  }
                }
              )) {
                ServerResponse.statusCode = 404;
                ServerResponse.end ();
                console.info (new Date().toISOString() + "\tdownload notfound: " + querystring.file + "\tremoteaddr: " + IncomingMessage.connection.remoteAddress);
              } else {
                contents = fs_readfilesync (path + "/" + querystring.file, { "encoding": "binary" } );
                if (!contents.length) {
                  ServerResponse.statusCode = 500;
                  ServerResponse.end ();
                  console.info (new Date().toISOString() + "\tdownload cantread: " + querystring.file + "\tremoteaddr: " + IncomingMessage.connection.remoteAddress);
                } else {
                  crypto_randombytes (16,
                    function (exception, buffer) {
                      if (exception) throw exception;
                      uniqueid = buffer.toString ("hex");
                      if (debug) {
                        ServerResponse.on ("close",
                          function () {
                            console.info (uniqueid + "\taborted");
                          }
                        );
                        ServerResponse.on ("finish",
                          function () {
                            console.info (uniqueid + "\tsent");
                          }
                        );
                        console.info (uniqueid + "\tdate: " + new Date().toISOString());
                        console.info (uniqueid + "\tremoteaddr: " + IncomingMessage.connection.remoteAddress);
                        console.info (uniqueid + "\tfile: " + querystring.file);
                      }
                      var istext = querystring.file.match (/\.(tsv)$/i);
                      ServerResponse.writeHead (200, { "content-type": (istext ? "text/plain" : "application/octet-stream"), "content-length": contents.length, "content-disposition": (istext ? "inline" : "attachment") + "; filename=\"" + querystring.file + "\"" } );
                      ServerResponse.end (contents, "binary");
                      if (debug) console.info (uniqueid + "\tdownload length: " + contents.length);
                    }
                  );
                }
              }
            }
            break;
          case "/graph":
            if (querystring.hostname && !querystring.hostname.match (/^[0-9a-z-]+$/i)) {
              ServerResponse.statusCode = 400;
              ServerResponse.end ();
              console.info (new Date().toISOString() + "\thostname invalid: " + IncomingMessage.url + "\tremoteaddr: " + IncomingMessage.connection.remoteAddress);
            } else {
              crypto_randombytes (16,
                function (exception, buffer) {
                  if (exception) throw exception;
                  uniqueid = buffer.toString ("hex");
                  ServerResponse.on ("close",
                    function () {
                      console.info (uniqueid + "\taborted");
                    }
                  );
                  ServerResponse.on ("finish",
                    function () {
                      console.info (uniqueid + "\tsent");
                    }
                  );
                  console.info (uniqueid + "\tdate: " + new Date().toISOString());
                  console.info (uniqueid + "\tremoteaddr: " + IncomingMessage.connection.remoteAddress);
                  if (querystring.hostname) console.info (uniqueid + "\thostname: " + querystring.hostname);
                  label = uniqueid + "\tgenerated";
                  console.time (label);
                  child_process_exec ("./rrd.sh graph '" + (querystring.hostname ? querystring.hostname : "") + "'", { encoding: "binary", maxBuffer: 10000000 },
                    function (error, stdout, stderr) {
                      console.timeEnd (label);
                      if (error !== null) {
                        ServerResponse.writeHead (500);
                        ServerResponse.end (error + stderr);
                        console.info (uniqueid + "\tgraph noexec: " + error.toString().replace (/[\r\n\t]+/gm, " "));
                      } else {
                        ServerResponse.writeHead (200, { "content-type": "image/png", "content-length": stdout.length, "content-disposition": "inline; filename=\"" + (querystring.hostname ? querystring.hostname : os_hostname ()) + "_" + new Date().getTime() + ".png\"" } );
                        ServerResponse.end (stdout, "binary");
                        console.info (uniqueid + "\tgraph length: " + stdout.length);
                      }
                    }
                  );
                }
              );
            }
            break;
          default:
            ServerResponse.statusCode = 400;
            ServerResponse.end ();
            console.info (new Date().toISOString() + "\tbad request: " + IncomingMessage.url + "\tremoteaddr: " + IncomingMessage.connection.remoteAddress);
            break;
        }
      }
    }
  }
).listen ($rrd_httpd_port);
server.on ("close",
  function () {
  }
);
HEREDOC
    chmod g+w "$rrd_httpdfile"
    nodejs "$rrd_httpdfile" >> "$rrd_logfile" ; exit
  ;;



  'update')

    rrd_sharedir="$rrd_sharedir/rrd"

    "$0" create
    "$0" flushcached

    if [ "$3" = 'once' ]
    then rrd_once='true'
    elif [ "$3" -eq "$3" ] 2>/dev/null
    then rrd_sleep=$3
    fi

    if test -f /etc/default/rrdcached
    then source /etc/default/rrdcached
    fi

    if ! [ -n "$RRDCACHED_ADDRESS" ]
    then
      if [ -n "$SOCKFILE" ]
      then export RRDCACHED_ADDRESS="unix:$SOCKFILE"
      elif [ -n "$NETWORK_OPTIONS" -a "$NETWORK_OPTIONS" == '-L' ]
      then export RRDCACHED_ADDRESS="localhost"
      fi
    fi

    rrd_checkcmd bc
    rrd_checkcmd curl
    rrd_checkcmd nodejs

    case "$2" in


      'idokep')

        rrd_ocr_opts="-l eng -psm 8 nobatch digits"


        rrd_users=( "${rrd_idokep_users[@]}" )

        rrd_checkcmd tesseract
        rrd_checkcmd convert

        for key in ${!rrd_users[@]}
        do
          user=${rrd_users[$key]}
          i=$((key+1))
          if [ $i -eq 1 ]
          then i=''
          fi

          curl=$(curl --silent --location "https://www.idokep.hu/automata/$user")
          err=$?
          if [ $err -gt 0 ]
          then
            if [ $err -ne 6 ]
            then echo -e "$(date '+%Y-%m-%d %H:%M:%S')\tERROR\tcurl\t$err" >> "$rrd_logfile"
            fi
          else
            userstr=$(echo "$curl" | grep -Pio "zemeltet.+?\:\s*\<a\s+href\=\'\#\'\s*onClick=\"\s*javascript\:\s*\{\s*window\.open\(\'\/adatlap\/$user\'")
            if [ -n "$userstr" ]
            then
              datestr="$(echo "$curl" | grep -Pio 'Utols.+?\s+m.+?r.+?s\:\s*\<b\>\s*\K(.+?)(?=\<\/b\>)')"
              if ! [ -n "$datestr" ]
              then echo -e "$(date '+%Y-%m-%d %H:%M:%S')\tERROR\tcontent not found\tdate" >> "$rrd_logfile"
              else
                if [ "$datestr" == '1970.  1. 01:00' ]
                then echo -e "$(date '+%Y-%m-%d %H:%M:%S')\tERROR\tautomata disabled\t$user" >> "$rrd_logfile"
                else
                  datestr=${datestr/#ma /today }
                  datestr=${datestr/#tegnap /yesterday }
                  stampthere=$(date --date="$datestr" '+%s' 2>/dev/null | tr -d [:space:])
                  if ! [ "$stampthere" -eq "$stampthere" ] 2>/dev/null
                  then echo -e "$(date '+%Y-%m-%d %H:%M:%S')\tERROR\tcontent invalid\tdate" >> "$rrd_logfile"
                  else
                    stamphere=$(date '+%s' 2>/dev/null | tr -d [:space:])
                    stampdiff=$((stamphere-stampthere))
                    if [ ${stampdiff#-} -lt 10800 ]
                    then
                      date=$(date --date="$datestr" '+%s' 2>/dev/null)
                      err=$?
                      if [ $err -gt 0 ]
                      then echo -e "$(date '+%Y-%m-%d %H:%M:%S')\tERROR\tcontent invalid\tdate" >> "$rrd_logfile"
                      else
                        enc="$(echo -n "$curl" | grep -Pio 'H.+?m.+?rs.+?klet\:\s*\<strong\>\<img\s+alt\=\"Embedded\s+Image\"\s*src\=\"data\:image\/png\;base64\,\K(.+?)(?=\"\>\s*.+?C)')"
                        if [ -n "$enc" ]
                        then
                          temp=$(echo -n "$enc" | base64 --decode | convert - -resize 400% -type grayscale -bordercolor white -border 10 tif:- 2>/dev/null | tesseract --tessdata-dir /usr/share/tesseract-ocr - - $rrd_ocr_opts 2>/dev/null)
                          err=$?
                          if [ $err -gt 0 ]
                          then echo -e "$(date '+%Y-%m-%d %H:%M:%S')\tERROR\tocr\t$err" >> "$rrd_logfile"
                          else
                            temp=$(echo "$temp" | tr -d [:space:])
                            if [ "$temp" != '-' -a "$temp" != '' ]
                            then
                              if ! printf '%0.1f' "$temp" >/dev/null 2>&1 \
                                || [ $(echo "$temp >= -89.2" | bc --mathlib 2>/dev/null) -ne 1 -o $(echo "$temp <= 56.7" | bc --mathlib 2>/dev/null) -ne 1 ]
                              then
                                echo -e "$(date '+%Y-%m-%d %H:%M:%S')\tERROR\tcontent invalid\ttemp\t$user" >> "$rrd_logfile"
                                file="debug/rrd-$1-ocr-error-$i-$date.png"
                                if ! test -e "$file"
                                then echo -n "$enc" | base64 --decode > "$file"
                                fi
                              else
                                if test -n "$rrd_debug"
                                then
                                  file="debug/rrd-$1-ocr-debug-$i-$date.png"
                                  if ! test -e "$file"
                                  then echo -n "$enc" | base64 --decode > "$file"
                                  fi
                                fi
                                file="$rrd_sharedir/rrd-update-data-outdoor-temp$i.tsv"
                                curr=$(echo -en "$date\t$temp")
                                prev=$(cat "$file" 2>/dev/null)
                                if [ "$curr" != "$prev" ]
                                then
                                  echo -n "$curr" > "$file"
                                  chmod g+w "$file"
                                fi
                              fi
                            fi
                          fi
                        fi
                        enc="$(echo -n "$curl" | grep -Pio 'P.+?ratartalom\:\s*\<strong\>\<img\s+alt\=\"Embedded\s+Image\"\s*src\=\"data\:image\/png\;base64\,\K(.+?)(?=\"\>\s*.+?)')"
                        if [ -n "$enc" ]
                        then
                          humid=$(echo -n "$enc" | base64 --decode | convert - -resize 400% -type grayscale -bordercolor white -border 10 tif:- 2>/dev/null | tesseract --tessdata-dir /usr/share/tesseract-ocr - - $rrd_ocr_opts 2>/dev/null)
                          err=$?
                          if [ $err -gt 0 ]
                          then echo -e "$(date '+%Y-%m-%d %H:%M:%S')\tERROR\tocr\t$err" >> "$rrd_logfile"
                          else
                            humid=$(echo "$humid" | tr -d [:space:])
                            if [ "$humid" != '-' -a "$humid" != '' ]
                            then
                              if ! printf '%0.0f' "$humid" >/dev/null 2>&1 \
                                || [ $(echo "$humid >= 0" | bc --mathlib 2>/dev/null) -ne 1 -o $(echo "$humid <= 100" | bc --mathlib 2>/dev/null) -ne 1 ]
                              then
                                echo -e "$(date '+%Y-%m-%d %H:%M:%S')\tERROR\tcontent invalid\thumid\t$user" >> "$rrd_logfile"
                                file="debug/rrd-$1-ocr-error-$i-$date-humid.png"
                                if ! test -e "$file"
                                then echo -n "$enc" | base64 --decode > "$file"
                                fi
                              else
                                if test -n "$rrd_debug"
                                then
                                  file="debug/rrd-$1-ocr-debug-$i-$date-humid.png"
                                  if ! test -e "$file"
                                  then echo -n "$enc" | base64 --decode > "$file"
                                  fi
                                fi
                                file="$rrd_sharedir/rrd-update-data-outdoor-humid$i.tsv"
                                curr=$(echo -en "$date\t$humid")
                                prev=$(cat "$file" 2>/dev/null)
                                if [ "$curr" != "$prev" ]
                                then
                                  echo -n "$curr" > "$file"
                                  chmod g+w "$file"
                                fi
                              fi
                            fi
                          fi
                        fi
                      fi
                    fi
                  fi
                fi
              fi
            fi
          fi
        done
      ;;


      'tplink')
        rrd_hostname='net'

        return="$(curl --silent --max-time 4 --user "$rrd_tplink_auth" --referer "http://$rrd_tplink_address" "http://$rrd_tplink_address/userRpm/StatusRpm.htm")"
        code=$?
        if [ $code -ne 0 ]
        then
          echo -e "$(date '+%Y-%m-%d %H:%M:%S')\tERROR\ttplink curl error $code" >> "$rrd_logfile"
        fi

        if input="$(echo "$return" | tr -s '[:space:]' ' ' | grep -oP '<SCRIPT language="javascript" type="text/javascript"> var statistList = new Array\( [0-9]+, [0-9]+, [0-9]+, [0-9]+, 0,0 \); </SCRIPT>' )" \
          && [ -n "$input" ]
        then
          read down up < <(echo "$input" | grep -oP '[0-9]+, ' | grep -oP '[0-9]+' | head -2 | tr '[:space:]' ' ' )
          if [ "$down" -eq "$down" -a "$down" -ge 0 -a "$up" -eq "$up" -a "$up" -ge 0 ] 2>/dev/null
          then
            rrd_probe='down'
            data="$down"
            rrd_update
            rrd_probe='up'
            data="$up"
            rrd_update
          fi
        fi

        rrd_probe='digi'
        input="$(echo "$return" | tr -s '[:space:]' ' ' | grep -oP '<SCRIPT language="javascript" type="text/javascript"> var wanPara = new Array\( .+?, "90\-F6\-52\-C2\-55\-8F", "[0-9\.]+", .+? \); </SCRIPT>' | grep -Po '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | head -1 )"
        if [ -z "$input" -o "$input" = '0.0.0.0' ]
        then input=0
        else input=100
        fi
        data="$input"
        rrd_update
      ;;


      'devices')

        case "$rrd_hostname" in

          'moli-beebox')

            if ! [ -n "$rrd_sleep" ]
            then rrd_sleep=60
            fi

            while [ "$rrd_wasonce" != 'true' ]
            do

              rrd_probe='hdd-temp'
              path="$rrd_sharedir/rrd-update-data-$rrd_probe.tsv"
              if input=$(cat $path 2>/dev/null) \
                && [ -n "$input" ] \
                && date=${input%%$'\t'*} \
                && [ "$date" -eq "$date" -a "$date" -gt 1 ] 2>/dev/null \
                && rest=${input#*$'\t'} \
                && data=${rest%%$'\t'*} \
                && [ -n "$data" ]
              then rrd_update "$date"
              fi

              rrd_probe='hdd-vid-temp'
              path="$rrd_sharedir/rrd-update-data-$rrd_probe.tsv"
              if input=$(cat $path 2>/dev/null) \
                && [ -n "$input" ] \
                && date=${input%%$'\t'*} \
                && [ "$date" -eq "$date" -a "$date" -gt 1 ] 2>/dev/null \
                && rest=${input#*$'\t'} \
                && data=${rest%%$'\t'*} \
                && [ -n "$data" ]
              then rrd_update "$date"
              fi

              rrd_probe='indoor-temp'
              # temperature sensor: PCSensor TEMPer v1.4 Microdia RDing TEMPerV1.4 0c45:7401 , source: http://www.dx.com/p/temper-usb-thermometer-temperature-recorder-for-pc-laptop-81105
              if input=$(./pcsensor 2>/dev/null) \
                && [ -n "$input" ] \
                && input=${input##* } \
                && data=${input%C*} \
                && [ $(echo "$data >= -89.2" | bc) -eq 1 -a $(echo "$data <= 56.7" | bc) -eq 1 ]
              then
                rrd_update
                file="$rrd_sharedir/rrd-update-data-$rrd_probe.tsv"
                echo -en "$(date '+%s')\t$data" > "$file"
                chmod g+w "$file"
              else
                if [ "$input" != 'r=0. expected 8.' -a "$input" != 'r=-19. expected 8.' ]
                then rrd_error
                fi
              fi

              rrd_probe='indoor-temp2'
              # temperature sensor: Xiaomi Mijia Thermohygrometer , source: https://github.com/sullin/hyg
              if input=$(./hyg 4c:65:a8:db:49:f9 2>/dev/null) \
                && [ -n "$input" ] \
                && data="$(echo "$input" | grep -P '^temp.value [0-9\.]+$')" \
                && [ -n "$data" ] \
                && data=${data#temp.value } \
                && [ $(echo "$data >= -89.2" | bc) -eq 1 -a $(echo "$data <= 56.7" | bc) -eq 1 ]
              then
                rrd_update
                file="$rrd_sharedir/rrd-update-data-$rrd_probe.tsv"
                echo -en "$(date '+%s')\t$data" > "$file"
                chmod g+w "$file"
              fi
              rrd_probe='indoor-humid'
              # humidity sensor: Xiaomi Mijia Thermohygrometer , source: https://github.com/sullin/hyg
              if [ -n "$input" ] \
                && data="$(echo "$input" | grep -P '^hum.value [0-9\.]+$')" \
                && [ -n "$data" ] \
                && data=${data#hum.value } \
                && [ $(echo "$data >= 0" | bc) -eq 1 -a $(echo "$data <= 100" | bc) -eq 1 ]
              then
                rrd_update
                file="$rrd_sharedir/rrd-update-data-$rrd_probe.tsv"
                echo -en "$(date '+%s')\t$data" > "$file"
                chmod g+w "$file"
              fi

              for key in ${!rrd_idokep_users[@]}
              do
                user=${rrd_idokep_users[$key]}
                i=$((key+1))
                if [ $i -eq 1 ]
                then i=''
                fi
                for type in 'temp' 'humid'
                do
                  rrd_probe="outdoor-$type$i"
                  path="$rrd_sharedir/rrd-update-data-$rrd_probe.tsv"
                  if input=$(cat $path 2>/dev/null) \
                    && [ -n "$input" ] \
                    && date=${input%%$'\t'*} \
                    && [ "$date" -eq "$date" -a "$date" -gt 1 ] 2>/dev/null \
                    && rest=${input#*$'\t'} \
                    && data=${rest%%$'\t'*} \
                    && [ -n "$data" ]
                  then rrd_update "$date"
                  fi
                done
              done

              if [ "$rrd_once" = 'true' ]
              then rrd_wasonce='true'
              else
                sleep "$rrd_sleep" &
                wait $!
              fi
            done
          ;;

          'moli-desktop')

            if ! [ -n "$rrd_sleep" ]
            then rrd_sleep=60
            fi

            while [ "$rrd_wasonce" != 'true' ]
            do

              rrd_probe='ssd-temp'
              path="$rrd_sharedir/rrd-update-data-$rrd_probe.tsv"
              if input=$(cat $path 2>/dev/null) \
                && [ -n "$input" ] \
                && date=${input%%$'\t'*} \
                && [ "$date" -eq "$date" -a "$date" -gt 1 ] 2>/dev/null \
                && rest=${input#*$'\t'} \
                && data=${rest%%$'\t'*} \
                && [ -n "$data" ]
              then rrd_update "$date"
              fi

              rrd_probe='hdd-dat-temp'
              path="$rrd_sharedir/rrd-update-data-$rrd_probe.tsv"
              if input=$(cat $path 2>/dev/null) \
                && [ -n "$input" ] \
                && date=${input%%$'\t'*} \
                && [ "$date" -eq "$date" -a "$date" -gt 1 ] 2>/dev/null \
                && rest=${input#*$'\t'} \
                && data=${rest%%$'\t'*} \
                && [ -n "$data" ]
              then rrd_update "$date"
              fi

              rrd_probe='indoor-temp'
              if input=$(curl --silent --connect-timeout 4 "$rrd_data_server&file=rrd-update-data-$rrd_probe.tsv") \
                && [ -n "$input" ] \
                && date=${input%%$'\t'*} \
                && [ "$date" -eq "$date" -a "$date" -gt 1 ] 2>/dev/null \
                && rest=${input#*$'\t'} \
                && data=${rest%%$'\t'*} \
                && [ $(echo "$data >= -89.2" | bc) -eq 1 -a $(echo "$data <= 56.7" | bc) -eq 1 ]
              then rrd_update "$date"
              fi

              rrd_probe='indoor-temp2'
              if input=$(curl --silent --connect-timeout 4 "$rrd_data_server&file=rrd-update-data-$rrd_probe.tsv") \
                && [ -n "$input" ] \
                && date=${input%%$'\t'*} \
                && [ "$date" -eq "$date" -a "$date" -gt 1 ] 2>/dev/null \
                && rest=${input#*$'\t'} \
                && data=${rest%%$'\t'*} \
                && [ $(echo "$data >= -89.2" | bc) -eq 1 -a $(echo "$data <= 56.7" | bc) -eq 1 ]
              then rrd_update "$date"
              fi
              rrd_probe='indoor-humid'
              if input=$(curl --silent --connect-timeout 4 "$rrd_data_server&file=rrd-update-data-$rrd_probe.tsv") \
                && [ -n "$input" ] \
                && date=${input%%$'\t'*} \
                && [ "$date" -eq "$date" -a "$date" -gt 1 ] 2>/dev/null \
                && rest=${input#*$'\t'} \
                && data=${rest%%$'\t'*} \
                && [ $(echo "$data >= 0" | bc) -eq 1 -a $(echo "$data <= 100" | bc) -eq 1 ]
              then rrd_update "$date"
              fi

              for key in ${!rrd_idokep_users[@]}
              do
                user=${rrd_idokep_users[$key]}
                i=$((key+1))
                if [ $i -eq 1 ]
                then i=''
                fi
                for type in 'temp' 'humid'
                do
                  rrd_probe="outdoor-$type$i"
                  file="rrd-update-data-$rrd_probe.tsv"
                  path="$rrd_sharedir/$file"
                  curl --silent --connect-timeout 4 --output "$path" "$rrd_data_server&file=$file"
                  chmod g+w "$path"

                  if input=$(cat $path 2>/dev/null) \
                    && [ -n "$input" ] \
                    && date=${input%%$'\t'*} \
                    && [ "$date" -eq "$date" -a "$date" -gt 1 ] 2>/dev/null \
                    && rest=${input#*$'\t'} \
                    && data=${rest%%$'\t'*} \
                    && [ -n "$data" ]
                  then rrd_update "$date"
                  fi
                done
              done

              if [ "$rrd_once" = 'true' ]
              then rrd_wasonce='true'
              else
                sleep "$rrd_sleep" &
                wait $!
              fi
            done
          ;;
        esac
      ;;


      'system')

        case "$rrd_hostname" in

          'moli-beebox')

            if ! [ -n "$rrd_sleep" ]
            then rrd_sleep=15
            fi

            while [ "$rrd_wasonce" != 'true' ]
            do

              rrd_probe='cpu-temp'
              # CPU: Intel Celeron J3455 , maximum temperature: 105 C , source: https://ark.intel.com/products/95594/Intel-Celeron-Processor-J3455-2M-Cache-up-to-2_3-GHz
              # detected sensors: coretemp-isa-0000
              path=/sys/devices/platform/coretemp.0/hwmon/hwmon1/temp1_input
              if input=$(cat $path) \
                && [ "$input" -eq "$input" ] 2>/dev/null \
                && data=$(echo "$input / 1000" | bc --mathlib) \
                && [ $(echo "$data > 1" | bc) -eq 1 -a $(echo "$data <= 115" | bc) -eq 1 ]
              then rrd_update
              else rrd_error
              fi

              rrd_probe='cpu1-temp'
              path=/sys/devices/platform/coretemp.0/hwmon/hwmon1/temp2_input
              if input=$(cat $path) \
                && [ "$input" -eq "$input" ] 2>/dev/null \
                && data=$(echo "$input / 1000" | bc --mathlib) \
                && [ $(echo "$data > 1" | bc) -eq 1 -a $(echo "$data <= 115" | bc) -eq 1 ]
              then rrd_update
              else rrd_error
              fi

              rrd_probe='cpu2-temp'
              path=/sys/devices/platform/coretemp.0/hwmon/hwmon1/temp3_input
              if input=$(cat $path) \
                && [ "$input" -eq "$input" ] 2>/dev/null \
                && data=$(echo "$input / 1000" | bc --mathlib) \
                && [ $(echo "$data > 1" | bc) -eq 1 -a $(echo "$data <= 115" | bc) -eq 1 ]
              then rrd_update
              else rrd_error
              fi

              rrd_probe='cpu3-temp'
              path=/sys/devices/platform/coretemp.0/hwmon/hwmon1/temp4_input
              if input=$(cat $path) \
                && [ "$input" -eq "$input" ] 2>/dev/null \
                && data=$(echo "$input / 1000" | bc --mathlib) \
                && [ $(echo "$data > 1" | bc) -eq 1 -a $(echo "$data <= 115" | bc) -eq 1 ]
              then rrd_update
              else rrd_error
              fi

              rrd_probe='cpu4-temp'
              path=/sys/devices/platform/coretemp.0/hwmon/hwmon1/temp5_input
              if input=$(cat $path) \
                && [ "$input" -eq "$input" ] 2>/dev/null \
                && data=$(echo "$input / 1000" | bc --mathlib) \
                && [ $(echo "$data > 1" | bc) -eq 1 -a $(echo "$data <= 115" | bc) -eq 1 ]
              then rrd_update
              else rrd_error
              fi

              rrd_probe='sys-temp1'
              # board: AsRock Beebox J3455 , https://www.asrock.com/nettop/Intel/Beebox%20Series%20(Apollo%20Lake)/index.asp
              # detected sensors: acpitz-virtual-0
              path=/sys/devices/virtual/hwmon/hwmon0/temp1_input
              if input=$(cat $path) \
                && [ "$input" -eq "$input" ] 2>/dev/null \
                && data=$(echo "$input / 1000" | bc --mathlib) \
                && [ $(echo "$data > 1" | bc) -eq 1 -a $(echo "$data <= 115" | bc) -eq 1 ]
              then rrd_update
              else rrd_error
              fi

              rrd_probe='sys-temp2'
              path=/sys/devices/virtual/thermal/thermal_zone0/temp
              if input=$(cat $path) \
                && [ "$input" -eq "$input" ] 2>/dev/null \
                && data=$(echo "$input / 1000" | bc --mathlib) \
                && [ $(echo "$data > 1" | bc) -eq 1 -a $(echo "$data <= 115" | bc) -eq 1 ]
              then rrd_update
              else rrd_error
              fi

              rrd_probe='sys-temp3'
              path=/sys/devices/virtual/thermal/thermal_zone1/temp
              if input=$(cat $path) \
                && [ "$input" -eq "$input" ] 2>/dev/null \
                && data=$(echo "$input / 1000" | bc --mathlib) \
                && [ $(echo "$data > 1" | bc) -eq 1 -a $(echo "$data <= 115" | bc) -eq 1 ]
              then rrd_update
              else rrd_error
              fi

              rrd_probe='net-google'
              if ping -q -n -c1 -w4 -W4 8.8.8.8 >/dev/null 2>&1
              then input=100
              else input=0
              fi
              data="$input"
              rrd_update

              if [ "$rrd_once" = 'true' ]
              then rrd_wasonce='true'
              else
                sleep "$rrd_sleep" &
                wait $!
              fi
            done
          ;;

          'moli-desktop')

            if ! [ -n "$rrd_sleep" ]
            then rrd_sleep=15
            fi

            while [ "$rrd_wasonce" != 'true' ]
            do

              rrd_probe='gpu-temp'
              # GPU: MSI Radeon RX 560 Aero ITX 4G OC , https://www.msi.com/Graphics-card/Radeon-RX-560-AERO-ITX-4G-OC.html
              # detected sensors: amdgpu-pci-0100
              path=/sys/devices/pci0000:00/0000:00:01.0/0000:01:00.0/hwmon/hwmon2/temp1_input
              if input=$(cat $path) \
                && [ "$input" -eq "$input" ] 2>/dev/null \
                && data=$(echo "$input / 1000" | bc --mathlib) \
                && [ $(echo "$data > 1" | bc) -eq 1 -a $(echo "$data <= 100" | bc) -eq 1 ]
              then rrd_update
              else rrd_error
              fi

              rrd_probe='cpu-temp'
              # CPU: Intel i5-6400 , maximum temperature: 71 C , source: https://ark.intel.com/products/88185/Intel-Core-i5-6400-Processor-6M-Cache-up-to-3_30-GHz
              # detected sensors: coretemp-isa-0000
              path=/sys/devices/platform/coretemp.0/hwmon/hwmon0/temp1_input
              if input=$(cat $path) \
                && [ "$input" -eq "$input" ] 2>/dev/null \
                && data=$(echo "$input / 1000" | bc --mathlib) \
                && [ $(echo "$data > 1" | bc) -eq 1 -a $(echo "$data <= 81" | bc) -eq 1 ]
              then rrd_update
              else rrd_error
              fi

              rrd_probe='cpu1-temp'
              path=/sys/devices/platform/coretemp.0/hwmon/hwmon0/temp2_input
              if input=$(cat $path) \
                && [ "$input" -eq "$input" ] 2>/dev/null \
                && data=$(echo "$input / 1000" | bc --mathlib) \
                && [ $(echo "$data > 1" | bc) -eq 1 -a $(echo "$data <= 81" | bc) -eq 1 ]
              then rrd_update
              else rrd_error
              fi

              rrd_probe='cpu2-temp'
              path=/sys/devices/platform/coretemp.0/hwmon/hwmon0/temp3_input
              if input=$(cat $path) \
                && [ "$input" -eq "$input" ] 2>/dev/null \
                && data=$(echo "$input / 1000" | bc --mathlib) \
                && [ $(echo "$data > 1" | bc) -eq 1 -a $(echo "$data <= 81" | bc) -eq 1 ]
              then rrd_update
              else rrd_error
              fi

              rrd_probe='cpu3-temp'
              path=/sys/devices/platform/coretemp.0/hwmon/hwmon0/temp4_input
              if input=$(cat $path) \
                && [ "$input" -eq "$input" ] 2>/dev/null \
                && data=$(echo "$input / 1000" | bc --mathlib) \
                && [ $(echo "$data > 1" | bc) -eq 1 -a $(echo "$data <= 81" | bc) -eq 1 ]
              then rrd_update
              else rrd_error
              fi

              rrd_probe='cpu4-temp'
              path=/sys/devices/platform/coretemp.0/hwmon/hwmon0/temp5_input
              if input=$(cat $path) \
                && [ "$input" -eq "$input" ] 2>/dev/null \
                && data=$(echo "$input / 1000" | bc --mathlib) \
                && [ $(echo "$data > 1" | bc) -eq 1 -a $(echo "$data <= 81" | bc) -eq 1 ]
              then rrd_update
              else rrd_error
              fi

              rrd_probe='sys-temp1'
              # board: Asrock Z170M Extreme4 , source: http://www.asrock.com/mb/Intel/Z170M%20Extreme4/
              # detected sensors: nct6791-isa-0290
              path=/sys/devices/platform/nct6775.656/hwmon/hwmon1/temp1_input
              if input=$(cat $path) \
                && [ "$input" -eq "$input" ] 2>/dev/null \
                && data=$(echo "$input / 1000" | bc --mathlib) \
                && [ $(echo "$data > 1" | bc) -eq 1 -a $(echo "$data <= 81" | bc) -eq 1 ]
              then rrd_update
              else rrd_error
              fi

              rrd_probe='sys-temp2'
              path=/sys/devices/platform/nct6775.656/hwmon/hwmon1/temp2_input
              if input=$(cat $path) \
                && [ "$input" -eq "$input" ] 2>/dev/null \
                && data=$(echo "$input / 1000" | bc --mathlib) \
                && [ $(echo "$data > 1" | bc) -eq 1 -a $(echo "$data <= 81" | bc) -eq 1 ]
              then rrd_update
              else rrd_error
              fi

              rrd_probe='sys-temp3'
              path=/sys/devices/platform/nct6775.656/hwmon/hwmon1/temp7_input
              if input=$(cat $path) \
                && [ "$input" -eq "$input" ] 2>/dev/null \
                && data=$(echo "$input / 1000" | bc --mathlib) \
                && [ $(echo "$data > 1" | bc) -eq 1 -a $(echo "$data <= 81" | bc) -eq 1 ]
              then rrd_update
              else rrd_error
              fi

              rrd_probe='sys-rpm2'
              path=/sys/devices/platform/nct6775.656/hwmon/hwmon1/fan5_input
              if input=$(cat $path) \
                && [ "$input" -eq "$input" -a "$input" -lt 12000 ] 2>/dev/null
              then
                data="$input"
                rrd_update
              else rrd_error
              fi

              if [ "$rrd_once" = 'true' ]
              then rrd_wasonce='true'
              else
                sleep "$rrd_sleep" &
                wait $!
              fi
            done
          ;;
        esac
      ;;


      'freq')

        if ! [ -n "$rrd_sleep" ]
        then rrd_sleep=5
        fi

        while [ "$rrd_wasonce" != 'true' ]
        do

          rrd_probe='ram-used'
          if input=$(free --bytes | grep -E '^Mem:' | grep -oE '[[:digit:]]+' | head -2 | tail -1) \
            && [ "$input" -eq "$input" -a "$input" -gt 1 ] 2>/dev/null
          then
            data="$input"
            rrd_update
          else rrd_error
          fi

          rrd_probe='swap-used'
          if input=$(free --bytes | grep -E '^Swap:' | grep -oE '[[:digit:]]+' | head -2 | tail -1) \
            && [ "$input" -eq "$input" ] 2>/dev/null
          then
            data="$input"
            rrd_update
          else rrd_error
          fi

          rrd_probe='cpu-load1'
          if input=$(cat /proc/loadavg | grep -oE '^[0-9\.]+') \
            && [ -n "$input" ]
          then
            data="$input"
            rrd_update
          else rrd_error
          fi

          if [ "$rrd_once" = 'true' ]
          then rrd_wasonce='true'
          else sleep "$rrd_sleep"
          fi
        done
      ;;


      'windows')

        if ! [ -n "$rrd_sleep" ]
        then rrd_sleep=5
        fi

        rrd_ohm_dir=/tmp/rrd
        mkdir --parents "$rrd_ohm_dir" 2>/dev/null
        chmod 770 "$rrd_ohm_dir"
        rrd_ohm_script="$rrd_ohm_dir/rrd-ohm.js"
        cat << 'HEREDOC' > "$rrd_ohm_script"
var stdin_str = "";
process.stdin.setEncoding( "utf8" );
process.stdin.on( "data",
  function( data ) {
    stdin_str += data.toString();
  }
);
process.stdin.on( "end",
  function() {

    function normalize( idiot_obj ) {
      var normal_obj = {};
      if( typeof idiot_obj.Text === "string" && typeof idiot_obj.Children === "object" && idiot_obj.Children.length ) {
        for( var child of idiot_obj.Children ) {
          if( typeof child.Text === "string" && typeof child.Children === "object" && child.Children.length ) {
            normal_obj[ child.Text ] = normalize( child );
          }
          if( typeof child.Text === "string" && typeof child.Value === "string" && child.Value ) {
            normal_obj[ child.Text ] = child.Value;
          }
        }
      }
      return normal_obj;
    }

    function object2lines( obj, path ) {
      var lines = "";
      if( typeof path === "undefined" ) path = "";
      for( var prop in obj ) {
        if( obj.hasOwnProperty( prop ) ) {
          var val = obj[prop];
          if( typeof val === "function" ) {
          } else if( typeof val === "object" ) {
            lines += object2lines( val, ( path ? path + "\t" : "") + prop );
          } else {
            lines += path + "\t" + prop + "\t" + val + "\n";
          }
        }
      }
      return lines;
    }

    process.stdout.write( object2lines( normalize( JSON.parse( stdin_str ) ) ) );

  }
);
HEREDOC
        chmod g+w "$rrd_ohm_script"

        while [ "$rrd_wasonce" != 'true' ]
        do

          ohm="$(curl --silent --connect-timeout 4 "$rrd_ohm_server/data.json" | nodejs "$rrd_ohm_script" 2>/dev/null)"
          if ! [ -n "$ohm" ]
          then
            if [ "$rrd_once" = 'true' ]
            then
              sleep $(( "$rrd_sleep" * 60 )) &
              wait $!
            fi
          else
            rrd_hostname=$(echo ${ohm%%$'\t'*} | tr [:upper:] [:lower:])

            rrd_probe='ram-used'
            if input="$(echo "$ohm" | grep -P '\tGeneric Memory\tData\tUsed Memory\t')" \
              && [ $? -eq 0 ] \
              && data="${input##*$'\t'}" \
              && data=${data/,/.} \
              && data=${data/ GB/*1024^3} \
              && data=$(echo "$data / 1" | bc) \
              && [ "$data" -eq "$data" -a "$data" -gt 1 ] 2>/dev/null
            then rrd_update
            else
              if [ "$data" != '-' ] ; then rrd_error ; fi
            fi

            rrd_probe='cpu-used'
            if input="$(echo "$ohm" | grep -P '\tLoad\tCPU Total\t')" \
              && [ $? -eq 0 ] \
              && data="${input##*$'\t'}" \
              && data=${data/,/.} \
              && data=${data% %} \
              && [ $(echo "$data >= 0" | bc) -eq 1 -a $(echo "$data <= 100" | bc) -eq 1 ]
            then rrd_update
            else
              if [ "$data" != '-' ] ; then rrd_error ; fi
            fi

            rrd_probe='gpu-used'
            if input="$(echo "$ohm" | grep -P '\tLoad\tGPU Core\t')" \
              && [ $? -eq 0 ] \
              && data="${input##*$'\t'}" \
              && data=${data/,/.} \
              && data=${data% %} \
              && [ $(echo "$data >= 0" | bc) -eq 1 -a $(echo "$data <= 100" | bc) -eq 1 ]
            then rrd_update
            else
              if [ "$data" != '-' ] ; then rrd_error ; fi
            fi

            rrd_probe='gpu-temp'
            if input="$(echo "$ohm" | grep -P '\tTemperatures\tGPU Core\t')" \
              && [ $? -eq 0 ] \
              && data="${input##*$'\t'}" \
              && [ "${data: -1}" == 'C' ] \
              && data=${data/,/.} \
              && data=${data% *} \
              && [ $(echo "$data > 1" | bc) -eq 1 -a $(echo "$data <= 100" | bc) -eq 1 ]
            then rrd_update
            else
              if [ "$data" != '-' ] ; then rrd_error ; fi
            fi

            rrd_probe='gpu-rpm'
            if input="$(echo "$ohm" | grep -P '\tFans\tGPU Fan\t')" \
              && [ $? -eq 0 ] \
              && data="${input##*$'\t'}" \
              && data=${data% RPM} \
              && [ "$data" -eq "$data" -a "$data" -lt 6000 ] 2>/dev/null
            then rrd_update
            else
              if [ "$data" != '-' ] ; then rrd_error ; fi
            fi

            rrd_probe='cpu-temp'
            if input="$(echo "$ohm" | grep -P '\tTemperatures\tCPU Package\t')" \
              && [ $? -eq 0 ] \
              && data="${input##*$'\t'}" \
              && [ "${data: -1}" == 'C' ] \
              && data=${data/,/.} \
              && data=${data% *} \
              && [ $(echo "$data > 1" | bc) -eq 1 -a $(echo "$data <= 81" | bc) -eq 1 ]
            then rrd_update
            else
              if [ "$data" != '-' ] ; then rrd_error ; fi
            fi

            rrd_probe='cpu1-temp'
            if input="$(echo "$ohm" | grep -P '\tTemperatures\tCPU Core #1\t')" \
              && [ $? -eq 0 ] \
              && data="${input##*$'\t'}" \
              && [ "${data: -1}" == 'C' ] \
              && data=${data/,/.} \
              && data=${data% *} \
              && [ $(echo "$data > 1" | bc) -eq 1 -a $(echo "$data <= 81" | bc) -eq 1 ]
            then rrd_update
            else
              if [ "$data" != '-' ] ; then rrd_error ; fi
            fi

            rrd_probe='cpu2-temp'
            if input="$(echo "$ohm" | grep -P '\tTemperatures\tCPU Core #2\t')" \
              && [ $? -eq 0 ] \
              && data="${input##*$'\t'}" \
              && [ "${data: -1}" == 'C' ] \
              && data=${data/,/.} \
              && data=${data% *} \
              && [ $(echo "$data > 1" | bc) -eq 1 -a $(echo "$data <= 81" | bc) -eq 1 ]
            then rrd_update
            else
              if [ "$data" != '-' ] ; then rrd_error ; fi
            fi

            rrd_probe='cpu3-temp'
            if input="$(echo "$ohm" | grep -P '\tTemperatures\tCPU Core #3\t')" \
              && [ $? -eq 0 ] \
              && data="${input##*$'\t'}" \
              && [ "${data: -1}" == 'C' ] \
              && data=${data/,/.} \
              && data=${data% *} \
              && [ $(echo "$data > 1" | bc) -eq 1 -a $(echo "$data <= 81" | bc) -eq 1 ]
            then rrd_update
            else
              if [ "$data" != '-' ] ; then rrd_error ; fi
            fi

            rrd_probe='cpu4-temp'
            if input="$(echo "$ohm" | grep -P '\tTemperatures\tCPU Core #4\t')" \
              && [ $? -eq 0 ] \
              && data="${input##*$'\t'}" \
              && [ "${data: -1}" == 'C' ] \
              && data=${data/,/.} \
              && data=${data% *} \
              && [ $(echo "$data > 1" | bc) -eq 1 -a $(echo "$data <= 81" | bc) -eq 1 ]
            then rrd_update
            else
              if [ "$data" != '-' ] ; then rrd_error ; fi
            fi

            rrd_probe='sys-rpm2'
            if input="$(echo "$ohm" | grep -P '\tASRock Z170M Extreme4\tNuvoton NCT6791D\tFans\tFan #5\t')" \
              && [ $? -eq 0 ] \
              && data="${input##*$'\t'}" \
              && data=${data% RPM} \
              && [ "$data" -eq "$data" -a "$data" -lt 12000 ] 2>/dev/null
            then rrd_update
            else
              if [ "$data" != '-' ] ; then rrd_error ; fi
            fi

            rrd_probe='sys-temp1'
            if input="$(echo "$ohm" | grep -P '\tASRock Z170M Extreme4\tNuvoton NCT6791D\tTemperatures\tTemperature #2\t')" \
              && [ $? -eq 0 ] \
              && data="${input##*$'\t'}" \
              && [ "${data: -1}" == 'C' ] \
              && data=${data/,/.} \
              && data=${data% *} \
              && [ $(echo "$data > 1" | bc) -eq 1 -a $(echo "$data <= 81" | bc) -eq 1 ]
            then rrd_update
            else
              if [ "$data" != '-' ] ; then rrd_error ; fi
            fi

            rrd_probe='sys-temp2'
            if input="$(echo "$ohm" | grep -P '\tASRock Z170M Extreme4\tNuvoton NCT6791D\tTemperatures\tTemperature #1\t')" \
              && [ $? -eq 0 ] \
              && data="${input##*$'\t'}" \
              && [ "${data: -1}" == 'C' ] \
              && data=${data/,/.} \
              && data=${data% *} \
              && [ $(echo "$data > 1" | bc) -eq 1 -a $(echo "$data <= 81" | bc) -eq 1 ]
            then rrd_update
            else
              if [ "$data" != '-' ] ; then rrd_error ; fi
            fi

            rrd_probe='sys-temp3'
            if input="$(echo "$ohm" | grep -P '\tASRock Z170M Extreme4\tNuvoton NCT6791D\tTemperatures\tCPU Core\t')" \
              && [ $? -eq 0 ] \
              && data="${input##*$'\t'}" \
              && [ "${data: -1}" == 'C' ] \
              && data=${data/,/.} \
              && data=${data% *} \
              && [ $(echo "$data > 1" | bc) -eq 1 -a $(echo "$data <= 81" | bc) -eq 1 ]
            then rrd_update
            else
              if [ "$data" != '-' ] ; then rrd_error ; fi
            fi

            rrd_probe='ssd-temp'
            if input="$(echo "$ohm" | grep -P '\tOCZ\-TRION150\tTemperatures\tTemperature\t')" \
              && [ $? -eq 0 ] \
              && data="${input##*$'\t'}" \
              && [ "${data: -1}" == 'C' ] \
              && data=${data/,/.} \
              && data=${data% *} \
              && [ $(echo "$data > 1" | bc) -eq 1 -a $(echo "$data <= 75" | bc) -eq 1 ]
            then rrd_update
            else
              if [ "$data" != '-' ] ; then rrd_error ; fi
            fi

            rrd_probe='indoor-temp'
            path="$rrd_sharedir/rrd-update-data-$rrd_probe.tsv"
            if input=$(cat $path 2>/dev/null) \
              && [ -n "$input" ] \
              && date=${input%%$'\t'*} \
              && [ "$date" -eq "$date" -a "$date" -gt 1 ] 2>/dev/null \
              && rest=${input#*$'\t'} \
              && data=${rest%%$'\t'*} \
              && [ -n "$data" ]
            then rrd_update "$date"
            fi

            rrd_probe='indoor-temp2'
            path="$rrd_sharedir/rrd-update-data-$rrd_probe.tsv"
            if input=$(cat $path 2>/dev/null) \
              && [ -n "$input" ] \
              && date=${input%%$'\t'*} \
              && [ "$date" -eq "$date" -a "$date" -gt 1 ] 2>/dev/null \
              && rest=${input#*$'\t'} \
              && data=${rest%%$'\t'*} \
              && [ -n "$data" ]
            then rrd_update "$date"
            fi
            rrd_probe='indoor-humid'
            path="$rrd_sharedir/rrd-update-data-$rrd_probe.tsv"
            if input=$(cat $path 2>/dev/null) \
              && [ -n "$input" ] \
              && date=${input%%$'\t'*} \
              && [ "$date" -eq "$date" -a "$date" -gt 1 ] 2>/dev/null \
              && rest=${input#*$'\t'} \
              && data=${rest%%$'\t'*} \
              && [ -n "$data" ]
            then rrd_update "$date"
            fi

            for key in ${!rrd_idokep_users[@]}
            do
              user=${rrd_idokep_users[$key]}
              i=$((key+1))
              if [ $i -eq 1 ]
              then i=''
              fi
              for type in 'temp' 'humid'
              do
                rrd_probe="outdoor-$type$i"
                path="$rrd_sharedir/rrd-update-data-$rrd_probe.tsv"
                if input=$(cat $path 2>/dev/null) \
                  && [ -n "$input" ] \
                  && date=${input%%$'\t'*} \
                  && [ "$date" -eq "$date" -a "$date" -gt 1 ] 2>/dev/null \
                  && rest=${input#*$'\t'} \
                  && data=${rest%%$'\t'*} \
                  && [ -n "$data" ]
                then rrd_update "$date"
                fi
              done
            done

          fi

          if [ "$rrd_once" = 'true' ]
          then rrd_wasonce='true'
          else sleep "$rrd_sleep"
          fi
        done
      ;;


    esac

  ;;



esac
