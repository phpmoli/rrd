#!/bin/bash

rrd_sharedir="/dev/shm/rrd"

file=.rrd-config.sh
if test -f "$file"
then source "$file"
fi

rrd_logfile="rrd-$1.log"

mkdir "$rrd_sharedir" 2>/dev/null
chown rrd.rrd "$rrd_sharedir"
chmod g+w "$rrd_sharedir"
touch "$rrd_sharedir/.placeholder"

case "$1" in
  'update')
    case "$2" in
      'devices')
        if [ "$3" = 'once' ]
        then rrd_once='true'
        elif [ "$3" -eq "$3" ] 2>/dev/null
        then rrd_sleep=$3
        else rrd_sleep=60
        fi

        function rrd_update {
          if [ "$rrd_once" = 'true' ]
          then echo -e "$HOSTNAME-$rrd_gauge\\tN\\t$data"
          fi
          file="$rrd_sharedir/rrd-update-data-$rrd_gauge.tsv"
          echo -en "$(date '+%s')\t$data" > "$file"
          chmod g+w "$file"
        }

        function rrd_error {
          echo -e "$(date '+%Y-%m-%d %H:%M:%S')\tERROR\t$rrd_gauge\t\"$data\"" | tee -a "$rrd_logfile" 1>&2
        }

        while [ "$rrd_wasonce" != 'true' ]
        do

          # moli-beebox
          rrd_gauge='hdd-temp'
          # HDD: Western Digital WD10JFCX , maximum temperature: 60 C , source: http://www.wdc.com/global/products/specs/?driveID=1311
          path=/dev/disk/by-id/ata-WDC_WD10JFCX-68N6GN0_WD-WX61E14LP943
          if test -e $path
          then
            if data=$(hddtemp --numeric --unit=C $path) \
              && [ "$data" -eq "$data" -a "$data" -gt 1 -a "$data" -le 70 ] 2>/dev/null
            then rrd_update
            else rrd_error
            fi
          fi

          rrd_gauge='hdd-vid-temp'
          # HDD: Toshiba MQ01ABD100 , maximum temperature: 55 C , source: https://toshiba.semicon-storage.com/us/product/storage-products/client-hdd/mq01abdxxx.html
          path='/dev/disk/by-id/usb-ATA_TOSHIBA_MQ01ABD1_0123456789ABCDE-0:0'
          if test -e $path
          then
            if input=$(smartctl --attributes $path | grep -E '^194\s+Temperature_Celsius\s+' | tr -s ' ') \
              && for i in {1..9}
                do input=${input#* }
                done \
              && data=${input%% *} \
              && [ "$data" -eq "$data" -a "$data" -gt 1 -a "$data" -le 65 ] 2>/dev/null
            then rrd_update
            else rrd_error
            fi
          fi

          rrd_gauge='hdd-dat-temp'
          # HDD: HGST Travelstar HTS541010A9E680 , maximum temperature: 60 C , source: https://www.hgst.com/products/hard-drives/travelstar-5k1000
          path=/dev/disk/by-id/ata-HGST_HTS541010A9E680_JD1009DM2L9EMK
          if test -e $path
          then
            if input=$(smartctl --attributes $path | grep -E '^194\s+Temperature_Celsius\s+' | tr -s ' ') \
              && for i in {1..9}
                do input=${input#* }
                done \
              && data=${input%% *} \
              && [ "$data" -eq "$data" -a "$data" -gt 1 -a "$data" -le 70 ] 2>/dev/null
            then rrd_update
            else rrd_error
            fi
          fi

          # moli-desktop
          rrd_gauge='ssd-temp'
          # SSD: OCZ TRN150-25SAT3-240G , maximum temperature: 65 C , source: http://ocz.com/consumer/trion-150-ssd/specifications
          path=/dev/disk/by-id/ata-OCZ-TRION150_261B31JNK1BU
          if test -e $path
          then
            if data=$(hddtemp --numeric --unit=C $path) \
              && [ "$data" -eq "$data" -a "$data" -gt 1 -a "$data" -le 75 ] 2>/dev/null
            then rrd_update
            else rrd_error
            fi
          fi

          if [ "$rrd_once" = 'true' ]
          then rrd_wasonce='true'
          else
            sleep "$rrd_sleep" &
            wait $!
          fi
        done
      ;;
    esac
  ;;
esac
