apt-get install bc rrdtool rrdcached imagemagick nodejs curl
adduser rrd
cp rrd.sh ~rrd/
cp rrd-elevated.sh ~root/bin/
cp rrd-crontab /etc/cron.d/rrd
cp rrdcached.conf /etc/default/rrdcached
tar xf rrd-gocr.tar ~rrd/
chown -R rrd.rrd ~rrd
echo reboot

