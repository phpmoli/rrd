#!/bin/bash

top -b -n 1 | tail -n +8 | head -1 | awk '{ print $1,$9 }' | while IFS=' ' read -r pid pcpu
do
  if [ -n "$pid" -a "$pid" -eq "$pid" ]
  then
    ps -p $pid --cumulative --format etimes:1=,args:1= | while IFS=' ' read etimes args
    do
      if [ -n "$etimes" -a "$etimes" -eq "$etimes" ]
      then
        if [ $etimes -gt 60 -a $(echo "$pcpu > 99" | bc --mathlib 2>/dev/null) -eq 1 ]
        then
          kill -sigkill $pid
          echo "killing: $args"
          nohup $args & >/dev/null
          echo "starting: $args"
        fi
      fi
    done
  fi
done
