rrd
===

bash script for rrdtool database creating, updating, graphing and a nodejs httpd for display

___

Windows desktop probing needs an [Open Hardware Monitor](http://openhardwaremonitor.org/) web server running.

___

![example graph](https://gitlab.com/phpmoli/rrd/raw/master/rrd-graph-example-server.png)
___

![example graph](https://gitlab.com/phpmoli/rrd/raw/master/rrd-graph-example-desktop.png)
